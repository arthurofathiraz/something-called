<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAffilation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable('affiliation')) {

            Schema::create('affiliation', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('course_id')->nullable();
                $table->integer('user_id');
                $table->integer('affiliation_user_id')->nullable();
                $table->text('order_id')->nullable();
                $table->string('total_amount', 191);
                $table->text('status')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('affiliation');
    }
}

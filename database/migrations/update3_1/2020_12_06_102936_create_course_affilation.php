<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourseAffilation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable('course_affiliation')) {

            Schema::create('course_affiliation', function (Blueprint $table) {
                $table->increments('id');
                $table->string('commision_type', 100);
                $table->string('amount', 191);
                $table->string('url', 1000);
                $table->integer('course_id')->unsigned()->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('course_affiliation');
    }
}

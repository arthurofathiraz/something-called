<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderAffilation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable('orders_affiliation')) {

            Schema::create('orders_affiliation', function (Blueprint $table) {
                $table->increments('id');
                $table->integer('order_id')->unsigned()->nullable();
                $table->integer('affiliation_user_id')->unsigned()->nullable();
                $table->string('amount', 191);
                $table->string('status', 191);
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders_affiliation');
    }
}

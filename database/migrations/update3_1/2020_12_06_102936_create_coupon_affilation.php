<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCouponAffilation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        if (!Schema::hasTable('coupon_affiliation')) {

            Schema::create('coupon_affiliation', function (Blueprint $table) {
                $table->increments('id');
                $table->string('code', 191);
                $table->string('distype', 100);
                $table->string('amount', 191);
                $table->integer('affiliation_user_id')->nullable();
                $table->integer('course_id')->unsigned()->nullable();
                $table->integer('parent_coupon_id')->unsigned()->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('coupon_affiliation');
    }
}

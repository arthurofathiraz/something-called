<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1"/>
    <title>Akademi Kuliner - Checkout {{ $course['title'] }} </title>
    <link rel="shortcut icon" href="img/favicon.png" type="image/png">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick-theme.css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css">
    <link rel="stylesheet" type="text/css"
          href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/zoa.css') }}">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style type="text/css">
        .swal2-popup {
            font-size: 1.6rem !important;
        }
    </style>
</head>

<body class="">
<div class="wrappage">
    <header id="header" class="header-v1">

    </header>
    <!-- /header -->

    <div class="container container-content">
        <ul class="breadcrumb v2" style="background-color: #ffffff">
        </ul>
    </div>

    <div class="check-out">
        <div class="container">
            <div class="titlell">
                <h2>Checkout</h2>
            </div>
            <div class="row" style="background-color: #ffffff">
                <div class="col-md-7 col-sm-7">
                    <div class="form-name">
                        <div class="content">
                            <h5>Punya Kode Diskon ?
                                <span class="text-danger" style="text-transform: uppercase;">
                                    Klik <a href="javascript:void(0)" id="link-coupon"
                                            style="text-decoration:underline"> disini </a> untuk masukkan kode
                                </span>
                            </h5>
                            <h5 id="div-title-login" style="display: none">Sudah mempunyai akun ?
                                <span class="text-danger" style="text-transform: uppercase;"> Klik
                                    <a href="javascript:void(0)" id="link-login" style="text-decoration:underline"> disini </a> untuk login
                                </span>
                            </h5>
                            @if(auth()->check())
                                <div id="div-title-user">
                                    <form id="div-form-logout"
                                          action="{{ url('product/' . $course["id"] . "/" . $slug ."/" . "checkout/logout") }}"
                                          method="post">
                                        @csrf
                                        <input type="hidden" name="url"
                                               value="{{ url()->current() }}">
                                        <h5>Anda login sebagai {{ auth()->user()->fname }},
                                            <span class="text-danger" style="text-transform: uppercase;">
                                    Klik <a href="javascript:void(0)" id="link-logout"
                                            style="text-decoration:underline"> disini </a> untuk logout
                                </span>
                                        </h5>
                                    </form>
                                </div>
                            @endif
                        </div>

                        @php
                            $unique_code = mt_rand(1, 99);
                            $total = 0;
                            if(!empty($course['discount_price'])) {
                                $total = $course['discount_price'] - $unique_code;
                            } else {
                                $total = $course['price'] - $unique_code;
                            }
                        @endphp

                        <div class="billing" id="div-form-login" style="display: none">
                            <form action="{{ url('product/' . $course["id"] . "/" . $slug ."/" . "checkout/login") }}"
                                  method="post">
                                @csrf
                                <input type="hidden" name="total"
                                       value="{{ \Illuminate\Support\Facades\Crypt::encrypt($total) }}">
                                <input type="hidden" name="affiliation_user_id"
                                       value="{{ request()->session()->get('akl.aff.uid', '') }}">
                                <input type="hidden" name="affiliation_user_email"
                                       value="{{ request()->session()->get('akl.aff.ueml', '') }}">
                                <input type="hidden" name="url"
                                       value="{{ url()->current() }}">

                                <label class="out">email <span class="text-danger">*</span></label><br>
                                <input type="email" name="email" required class="district">
                                <label class="out">password <span class="text-danger">*</span></label><br>
                                <input type="password" name="password" required class="district">
                            </form>
                        </div>

                        <div class="billing" id="div-form-input" style="display: none">
                            <h2 style="font-size: 26px; padding-bottom:20px;font-weight: bold">informasi pribadi</h2>
                            <form id="form-input"
                                  action="{{ url('product/' . $course["id"] . "/" . $slug ."/" . "checkout") }}"
                                  method="post">
                                @csrf
                                <input type="hidden" name="total"
                                       value="{{ \Illuminate\Support\Facades\Crypt::encrypt($total) }}">
                                <input type="hidden" name="affiliation_user_id"
                                       value="{{ request()->session()->get('akl.aff.uid', '') }}">
                                <input type="hidden" name="affiliation_user_email"
                                       value="{{ request()->session()->get('akl.aff.ueml', '') }}">
                                <input type="hidden" name="url"
                                       value="{{ url()->current() }}">

                                <label class="out">nama lengkap <span class="text-danger">*</span></label><br>
                                <input type="text" name="name" required class="district">
                                <label class="out">email <span class="text-danger">*</span></label><br>
                                <input type="email" name="email" required class="district">
                                <label class="out">password <span class="text-danger">*</span></label><br>
                                <input type="password" name="password" required class="district">
                                <label class="out">no. handphone <span class="text-danger">*</span></label><br>
                                <input type="text" name="mobile" required class="district">
                            </form>
                        </div>

                        <div class="billing" id="div-form-input-login" style="display: none">
                            <h2 style="font-size: 26px; padding-bottom:20px;font-weight: bold">informasi pribadi</h2>
                            <form id="form-input-login"
                                  action="{{ url('product/' . $course["id"] . "/" . $slug ."/" . "checkout") }}"
                                  method="post">
                                @csrf
                                <input type="hidden" name="total"
                                       value="{{ \Illuminate\Support\Facades\Crypt::encrypt($total) }}">
                                <input type="hidden" name="affiliation_user_id"
                                       value="{{ request()->session()->get('akl.aff.uid', '') }}">
                                <input type="hidden" name="affiliation_user_email"
                                       value="{{ request()->session()->get('akl.aff.ueml', '') }}">
                                <input type="hidden" name="url"
                                       value="{{ url()->current() }}">
                            </form>
                        </div>
                    </div>
                </div>

                <div class="col-md-5 col-sm-5">
                    <div class="order">
                        <div class="content-order">
                            <div class="table">
                                <table>
                                    <caption>pesanan anda</caption>
                                    <thead>
                                    <tr>
                                        <th>produk</th>
                                        <th></th>
                                        <th>total</th>

                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>
                                            @if(!empty($course['preview_image']))
                                                <img class="left" style="height: 25px;width: 25px; margin-right: 5px"
                                                     src="{{ asset('images/course')  . '/' . $course['preview_image'] }}"/>
                                            @endif
                                            {{ $course['title'] }}
                                        </td>
                                        <td><i class="fa fa-times" aria-hidden="true"></i>1</td>
                                        <td class="text-right">
                                            @if(!empty($course['discount_price']))
                                                {{ number_format($course['discount_price']) }}
                                            @else
                                                {{ number_format($course['price']) }}
                                            @endif
                                        </td>

                                    </tr>
                                    <hr/>
                                    <tr>
                                        <td>Kode unik</td>
                                        <td></td>
                                        <td class="text-right">
                                            {{ number_format($unique_code) }}
                                        </td>
                                    </tr>

                                    </tbody>
                                </table>
                            </div>
                            <div class="content-total">
                                <div class="total">
                                    <h5 class="sub-total">sub total</h5>
                                    <h5 class="prince">Rp. {{ number_format($total) }}</h5>
                                </div>

                                <div class="payment">
                                    <input type="radio" name="gender" class="so1" checked><span
                                        style="font-size: 16px;color:#494949; font-weight: bold"> Pembayaran Online (midtrans)</span>
                                    <p style="padding-left: 20px;padding-bottom: 20px">Pembayaran online menggunakan
                                        transfer bank, Gopay, QRIS dan lain-lain.</p>
                                    <p>
                                        <span class="text-success">
                                            <i class="fa fa-check"></i> Secure 100%
                                        </span>
                                    </p>

                                </div>
                                <div class="place-ober">
                                    <button class="ober" id="link-order">place order</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="newsletter">
    <div class="container">
        <div class="row">
            <div class="col-md-12 text-center">
                <p><img src="https://member.dcendes.co.id/wp-content/plugins/sejoli/public/img/shield.png"/> Informasi
                    pribadi anda aman</p>
            </div>
        </div>
    </div>
</div>
<!-- EndContent -->

<!-- Footer -->
<footer class="footer v2">
    <div class="container">
        <div class="f-content bd-top">

        </div>
    </div>
</footer>
<!-- End Footer -->


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
<script src="{{ asset('js/zoa.js') }}"></script>
<script type="text/javascript">
    function showNotification(types, title, text) {
        Swal.fire({
            title: title,
            html: text,
            type: types,
            confirmButtonText: 'Okay',
            timer: 3000,
            icon: types,
        })
    }
</script>
{{--  session response  --}}
@if (!empty(Session::get('message')))
    @include('affiliation.notification_success')
@elseif (!empty(Session::get('exception')))
    @include('affiliation.notification_error')
@endif
<script type="text/javascript">
    $(document).ready(function () {

        // repeat social proof order
        var order_count = 0;
        var total_order = '{{ count($social_proof) }}';
        var data_order = @php echo json_encode($social_proof); @endphp

        setInterval(function () {
            if (order_count == total_order) {
                order_count = 0;
            }

            var html = "<strong class='left' style='margin-left: 5px'>" + data_order[order_count]['user']['fname'] + " " + data_order[order_count]['user']['lname'] + "</strong>";
            html += " membeli <strong>" + data_order[order_count]['courses']['title'] + "</strong><br/>";
            html += " <span class='left' style='font-size: 10px';text-align:left;color:#fff000;margin-top:2px>" + data_order[order_count]['pembelian'] + "</span>";

            Swal.fire({
                html: html,
                position: 'top-end',
                timer: 3000,
                toast: true,
                showConfirmButton: false,
                imageUrl: "{{ asset('images/course') }}" + "/" + data_order[order_count]['courses']['preview_image'],
                imageWidth: 25,
                imageHeight: 25,
            });

            order_count++;
        }, 10000);

        // check auth
        var is_login = '{{ auth()->check() }}';
        if (is_login != '1') {
            $('#div-form-input').show();
            $('#div-title-login').show();
        }

        // login form
        $(document).on('click', '#link-login', function () {
            var html = $('#div-form-login').html();

            var dialog = bootbox.dialog({
                message: html,
                buttons: {
                    cancel: {
                        label: "Cancel",
                        className: 'btn-warning'
                    },
                    ok: {
                        label: "Submit",
                        className: 'btn-primary',
                        callback: function () {
                            var forms = $(dialog.find('form'));
                            forms.submit();
                        }
                    }
                }
            });
        });

        // logout form
        $(document).on('click', '#link-logout', function () {
            var forms = $('#div-form-logout');
            forms.submit();
        });

        // order form
        $(document).on('click', '#link-order', function () {
                @if(auth()->check())

            var forms = $('#form-input-login');
            forms.submit();

                @else

            var forms = $('#form-input');
            forms.submit();

            @endif
        });
    });
</script>
</body>

</html>

@extends("admin/layouts.master")
@section('title','Afiliasi Link')
@section("body")

    <section class="content">
        @include('admin.message')
        <div class="row">
            <div class="col-xs-12">
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Afiliasi Link</h3>

                    </div>
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-10">
                                <select class="form-control" id="select-course-link">
                                    <option class="form-control" disabled>--Pilih kursus--</option>
                                    @foreach($course as $cou)
                                        @if(isset($cou->affiliation))
                                            <option class="form-control" data-id="{{ $cou->id }}"
                                                    data-slug="{{ str_slug($cou->title) }}">{{ $cou->title }}</option>
                                        @endif
                                    @endforeach
                                </select>
                            </div>
                            <div class="col-md-2">
                                <a href="#" class="btn btn-primary" id="btn-generate-link">Generate</a>
                            </div>
                        </div>
                        <br/>
                        <div id="div-info-link">
                            <div class="row">
                                <div class="col-md-12">
                                    <strong>Halaman Penjualan / Sales Page</strong><br/>
                                    Link menuju ke halaman landing / sales page
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="input-product-link" readonly>
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-info" id="btn-product-copy" data-clipboard-text=""><i
                                            class="fa fa-copy"></i>
                                        Copy
                                    </button>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                            <br/>
                            <div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <strong>Halaman Checkout</strong><br/>
                                        Link menuju ke halaman checkout
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="input-checkout-link" readonly>
                                </div>
                                <div class="col-md-2">
                                    <button class="btn btn-info" id="btn-checkout-copy" data-clipboard-text=""><i
                                            class="fa fa-copy"></i>
                                        Copy
                                    </button>
                                </div>
                                <div class="col-md-2">
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-12">
                                    <h3><strong>Tambah Parameter ke Link Affiliasi Anda</strong></h3>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-12">
                                    <h4><strong>Parameter akuisisi data</strong></h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="select-platform">Platform</label>
                                    <select class="form-control" name="platform" id="select-platform">
                                        <option value="" disabled>--Pilih Platform--</option>
                                        <option value="fb">Facebook</option>
                                        <option value="ins">Instagram</option>
                                        <option value="insto">Instagram Story</option>
                                        <option value="what">Whatsapp</option>
                                        <option value="email">Email</option>
                                        <option value="line">Line</option>
                                    </select>
                                </div>
                                <div class="col-md-6">
                                    <label for="input-platform">Parameter</label>
                                    <input type="text" class="form-control" id="input-platform"
                                           placeholder="Untuk ID, anda bisa isi dengan identifikasi apapun." required>
                                </div>
                            </div>
                            <br/>
                            <div class="row">
                                <div class="col-md-12">
                                    <a href="#" class="btn btn-primary" id="btn-param-link">Tambahkan Parameter</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        </div>
    </section>
@endsection

@section('stylesheets')
    <style type="text/css">
        .swal2-popup {
            font-size: 1.6rem !important;
        }
    </style>
@endsection

@section('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.6/clipboard.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@10"></script>
    <script type="text/javascript">
        var link = '';

        $(document).ready(function () {
            var clipboard = new ClipboardJS('.btn');

            clipboard.on('success', function (e) {
                Swal.fire({
                    html: "Berhasil di copy.",
                    timer: 3000,
                    toast: true,
                    showConfirmButton: false,
                });

                e.clearSelection();
            });

            $(document).on('click', '#btn-generate-link', function () {
                var select_option = $('select[id="select-course-link"] option').filter(':selected');
                link = "{{ env('APP_URL') . "/affiliation/" . auth()->user()->id . "/" }}" + select_option.attr('data-id') + "/" +
                    select_option.attr('data-slug');

                $('#div-info-link').show();

                // set link value
                $('#input-product-link').attr('value', link);
                $('#input-checkout-link').attr('value', link + "/gotocheckout");

                // set link btn
                $('#btn-product-copy').attr('data-clipboard-text', link);
                $('#btn-checkout-copy').attr('data-clipboard-text', link + "/gotocheckout");
            });

            $(document).on('click', '#btn-param-link', function () {
                var param = $('#select-platform').val();
                var param_value = $('#input-platform').val();

                // set link value
                $('#input-product-link').attr('value', link + "?utm_source=" + param + "&utm_media=" + param_value);
                $('#input-checkout-link').attr('value', link + "/gotocheckout" + "?utm_source=" + param + "&utm_media=" + param_value);

                // set link btn
                $('#btn-product-copy').attr('data-clipboard-text', link + "?utm_source=" + param + "&utm_media=" + param_value);
                $('#btn-checkout-copy').attr('data-clipboard-text', link + "/gotocheckout" + "?utm_source=" + param + "&utm_media=" + param_value);
            });
        });
    </script>
@endsection






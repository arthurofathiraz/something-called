<section class="content">

    <div class="row">
        <div class="col-md-12">

            <div class="box-header with-border">
                <h3 class="box-title">Ubah Affiliasi</h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <form enctype="multipart/form-data" id="demo-form" method="post"
                          action="{{url('courseaffiliation/update/'.$cor->id)}}" data-parsley-validate
                          class="form-horizontal form-label-left">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}

                        @php
                            $amount = '';
                            $commision_type = '';
                            $url = '';

                            $course_affiliation = \App\CourseAffiliation::where('course_id', $cor->id)->first();
                            if ($course_affiliation) {
                                $amount = $course_affiliation->amount;
                                $commision_type = $course_affiliation->commision_type;
                                $url = $course_affiliation->url;
                            }
                        @endphp

                        <div class="row">
                            <div class="col-md-6">
                                <label for="amount">Besar Komisi (Rp.) :</label>
                                <input type="number" class="form-control " name="amount" id="amount"
                                       value="{{ $amount }}">
                            </div>
                            <div class="col-md-6">
                                <label for="commision_type">Tipe Komisi :</label>
                                <select name="commision_type" class="form-control" id="commision_type">
                                    <option value="" disabled>
                                        -- Pilih Tipe --
                                    </option>
                                    <option value="percent" {{ ($commision_type == 'percent') ? 'selected' : '' }}>
                                        Persentase
                                    </option>
                                    <option value="fixed" {{ ($commision_type == 'fixed') ? 'selected' : '' }}>Nilai
                                        Tetap
                                    </option>
                                </select>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <label for="amount">Landing Page</label>
                                <input type="text" class="form-control " name="url" id="url"
                                       value="{{ $url }}" placeholder="https://akademikuliner.com">
                            </div>
                        </div>

                        <br>

                        <div class="box-footer">
                            <button type="submit"
                                    class="btn btn-lg col-md-4 btn-primary">{{ __('adminstaticword.Save') }}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>

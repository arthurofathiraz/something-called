<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Course;
use App\Currency;
use App\InstructorSetting;
use App\Mail\SendOrderMail;
use App\Notifications\UserEnroll;
use App\Order;
use App\PendingPayout;
use App\Setting;
use App\User;
use App\Wishlist;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Validator;
use Midtrans\Config as MidtransConfig;
use Midtrans\Snap;

class MidtransController extends Controller
{
    public function __construct()
    {
        /** Midtrans config **/
        // get data setting
        $setting = Setting::query()->first();

        /** Midtrans config **/
        if ($setting->enable_midtrans_production == 1) {
            // Set your Merchant Server Key
            MidtransConfig::$serverKey = env('MIDTRANS_SERVER_KEY_PRODUCTION');
            // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
            MidtransConfig::$isProduction = env('MIDTRANS_IS_PRODUCTION_PRODUCTION');
            // Set sanitization on (default)
            MidtransConfig::$isSanitized = env('MIDTRANS_IS_SANITIZED_PRODUCTION');
            // Set 3DS transaction for credit card to true
            MidtransConfig::$is3ds = env('MIDTRANS_IS_3DS_PRODUCTION');
        } else {
            // Set your Merchant Server Key
            MidtransConfig::$serverKey = env('MIDTRANS_SERVER_KEY');
            // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
            MidtransConfig::$isProduction = env('MIDTRANS_IS_PRODUCTION');
            // Set sanitization on (default)
            MidtransConfig::$isSanitized = env('MIDTRANS_IS_SANITIZED');
            // Set 3DS transaction for credit card to true
            MidtransConfig::$is3ds = env('MIDTRANS_IS_3DS');
        }
    }

    public function pay(Request $request)
    {
        // untuk midtrans
        $midtrans_order_id = create_transaction_id();

        $list_order_item = [];
        $total_amount = Crypt::decrypt($request->amount);

        $currency = Currency::first();
        $carts = Cart::where('user_id', Auth::User()->id)->get();

        foreach ($carts as $cart) {
            if ($cart->offer_price != 0) {
                $pay_amount = $cart->offer_price;
            } else {
                $pay_amount = $cart->price;
            }

            if ($cart->disamount != 0 || $cart->disamount != NULL) {
                $cpn_discount = $cart->disamount;
            } else {
                $cpn_discount = '';
            }


            $lastOrder = Order::orderBy('created_at', 'desc')->first();

            if (!$lastOrder) {
                // We get here if there is no order at all
                // If there is no number set it to 0, which will be 1 at the end.
                $number = 0;
            } else {
                $number = substr($lastOrder->order_id, 3);
            }

            if ($cart->type == 1) {
                $bundle_id = $cart->bundle_id;
                $bundle_course_id = $cart->bundle->course_id;
                $course_id = NULL;
                $duration = NULL;
                $instructor_payout = 0;
                $instructor_id = $cart->bundle->user_id;

                if ($cart->bundle->duration_type == "m") {

                    if ($cart->bundle->duration != NULL && $cart->bundle->duration != '') {
                        $days = $cart->bundle->duration * 30;
                        $todayDate = date('Y-m-d');
                        $expireDate = date("Y-m-d", strtotime("$todayDate +$days days"));
                    } else {
                        $todayDate = NULL;
                        $expireDate = NULL;
                    }
                } else {

                    if ($cart->bundle->duration != NULL && $cart->bundle->duration != '') {
                        $days = $cart->bundle->duration;
                        $todayDate = date('Y-m-d');
                        $expireDate = date("Y-m-d", strtotime("$todayDate +$days days"));
                    } else {
                        $todayDate = NULL;
                        $expireDate = NULL;
                    }

                }
            } else {

                if ($cart->courses->duration_type == "m") {

                    if ($cart->courses->duration != NULL && $cart->courses->duration != '') {
                        $days = $cart->courses->duration * 30;
                        $todayDate = date('Y-m-d');
                        $expireDate = date("Y-m-d", strtotime("$todayDate +$days days"));
                    } else {
                        $todayDate = NULL;
                        $expireDate = NULL;
                    }
                } else {

                    if ($cart->courses->duration != NULL && $cart->courses->duration != '') {
                        $days = $cart->courses->duration;
                        $todayDate = date('Y-m-d');
                        $expireDate = date("Y-m-d", strtotime("$todayDate +$days days"));
                    } else {
                        $todayDate = NULL;
                        $expireDate = NULL;
                    }

                }


                $setting = InstructorSetting::first();

                if ($cart->courses->instructor_revenue != NULL) {
                    $x_amount = $pay_amount * $cart->courses->instructor_revenue;
                    $instructor_payout = $x_amount / 100;
                } else {

                    if (isset($setting)) {
                        if ($cart->courses->user->role == "instructor") {
                            $x_amount = $pay_amount * $setting->instructor_revenue;
                            $instructor_payout = $x_amount / 100;
                        } else {
                            $instructor_payout = 0;
                        }

                    } else {
                        $instructor_payout = 0;
                    }
                }

                $bundle_id = NULL;
                $course_id = $cart->course_id;
                $bundle_course_id = NULL;
                $duration = $cart->courses->duration;
                $instructor_id = $cart->courses->user_id;
            }

            // add order id
            $order_id = '#' . sprintf("%08d", intval($number) + 1);

            // add order item
            $list_order_item[] = [
                'id' => $course_id,
                'price' => $pay_amount,
                'quantity' => 1,
                'name' => $cart->courses->title,
            ];

            $created_order = Order::create([
                    'course_id' => $course_id,
                    'user_id' => Auth::User()->id,
                    'instructor_id' => $instructor_id,
                    'order_id' => $order_id,
                    'transaction_id' => str_random(32),
                    'payment_method' => 'OnlinePayment',
                    'total_amount' => $pay_amount,
                    'coupon_discount' => $cpn_discount,
                    'currency' => $currency->currency,
                    'currency_icon' => $currency->icon,
                    'duration' => $duration,
                    'enroll_start' => $todayDate,
                    'enroll_expire' => $expireDate,
                    'status' => 0,
                    'bundle_id' => $bundle_id,
                    'bundle_course_id' => $bundle_course_id,
                    'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                    'midtrans_transaction_id' => $midtrans_order_id,
                ]
            );


            $created_order->save();


            Wishlist::where('user_id', Auth::User()->id)->where('course_id', $cart->course_id)->delete();

            Cart::where('user_id', Auth::User()->id)->where('course_id', $cart->course_id)->delete();

            if ($instructor_payout != 0) {
                if ($created_order) {
                    if ($cart->type == 0) {
                        if ($cart->courses->user->role == "instructor") {
                            $created_payout = PendingPayout::create([
                                    'user_id' => $cart->courses->user_id,
                                    'course_id' => $cart->course_id,
                                    'order_id' => $created_order->id,
                                    'transaction_id' => $created_order->transaction_id,
                                    'total_amount' => $pay_amount,
                                    'currency' => $currency->currency,
                                    'currency_icon' => $currency->icon,
                                    'instructor_revenue' => $instructor_payout,
                                    'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                                    'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                                ]
                            );
                        }
                    }
                }
            }

            if ($created_order) {
                if (env('MAIL_USERNAME') != null) {
                    try {

                        /*sending email*/
                        $x = 'You are successfully enrolled in a course';
                        $order = $created_order;
                        Mail::to(Auth::User()->email)->send(new SendOrderMail($x, $order));


                    } catch (\Swift_TransportException $e) {
                        Session::flash('deleted', trans('flash.PaymentMailError'));
                        return redirect('confirmation');
                    }
                }
            }

            if ($cart->type == 0) {

                if ($created_order) {
                    // Notification when user enroll
                    $cor = Course::where('id', $cart->course_id)->first();

                    $course = [
                        'title' => $cor->title,
                        'image' => $cor->preview_image,
                    ];

                    $enroll = Order::where('course_id', $cart->course_id)->get();

                    if (!$enroll->isEmpty()) {
                        foreach ($enroll as $enrol) {
                            $user = User::where('id', $enrol->user_id)->get();
                            Notification::send($user, new UserEnroll($course));
                        }
                    }
                }
            }

            // bikin snap redirect ke midtrans

            // transaction midtrans
            $transaction_details = array(
                'order_id' => $midtrans_order_id,
                'gross_amount' => $total_amount, // no decimal allowed for creditcard
            );

            // item midtrans
            $item_details = $list_order_item;

            // get user
            $user = User::where('id', Auth::User()->id)->first();

            // billing address midtrans
            $billing_address = array(
                'first_name' => $user->fname,
                'last_name' => $user->lname,
                'address' => $user->address,
                'phone' => $user->mobile,
                'email' => $user->email,
                'country_code' => 'IDN'
            );

            // customer details midtrans
            $customer_details = array(
                'first_name' => $user->fname,
                'last_name' => $user->lname,
                'address' => $user->address,
                'phone' => $user->mobile,
                'email' => $user->email,
                'country_code' => 'IDN'
            );


            // midtrans params
            $params = array(
                'transaction_details' => $transaction_details,
                'customer_details' => $customer_details,
                'item_details' => $item_details,
            );

            // Get Snap Payment Page URL
            $paymentUrl = Snap::createTransaction($params)->redirect_url;

            try {
                // Get Snap Payment Page URL
                $paymentUrl = Snap::createTransaction($params)->redirect_url;

                // Redirect to Snap Payment Page
                return redirect()->away($paymentUrl);
            } catch (Exception $e) {
                dd($e->getMessage());
            }

            return redirect('confirmation');
        }
    }

    public function notification(Request $request)
    {
        //VALIDASI
        $rule = [
            'transaction_id' => 'required',
            'transaction_status' => 'required',
            'fraud_status' => 'nullable',
            'payment_type' => 'required',
            'order_id' => 'required',
        ];

        // validate
        $validator = Validator::make($request->all(), $rule);
        if ($validator->fails()) {
            return response()->json(response_error(implode(", ", $validator->messages()->all())), 400);
        }

        // get input
        $input = $request->all();

        // find order
        $orders = Order::where('midtrans_transaction_id', '=', $input['order_id'])->get();
        if (!$orders) {
            return response()->json(response_error('Order tidak di temukan.'), 400);
        }

        // loop each order
        foreach ($orders as $order) {

            // data update
            $data_update = [
                'status' => $order->status,
                'midtrans_transaction_id' => $input['order_id'],
                'midtrans_response' => json_encode($input),
            ];

            // check transaction status
            switch ($input['transaction_status']) {
                case 'capture':
                    $data_update['status'] = 1;
                    break;
                case 'settlement':
                    $data_update['status'] = 1;
                    break;
                case 'pending':
                    $data_update['status'] = 0;
                    break;
                case 'deny':
                    $data_update['status'] = 0;
                    break;
                case 'cancel':
                    $data_update['status'] = 0;
                    break;
                case 'expire':
                    $data_update['status'] = 0;
                    break;
            }

            // check fraud status
            if (isset($input['fraud_status'])) {

                // check fraud
                switch ($input['fraud_status']) {
                    case 'accept':
                        if ($input['transaction_status'] == 'capture') {
                            $data_update['status'] = 1;
                        } else if ($input['transaction_status'] == 'cancel') {
                            $data_update['status'] = 0;
                        }
                        break;

                    case 'challenge':
                        if ($input['transaction_status'] == 'capture') {
                            $data_update['status'] = 0;
                        } else if ($input['transaction_status'] == 'cancel') {
                            $data_update['status'] = 1;
                        }
                        break;

                    case 'deny':
                        $data_update['status'] = 0;
                }
            }

            // try update order data
            $update = $order->update($data_update);
            if (!$update) {
                return response()->json(response_error('Gagal mengubah order.'), 400);
            }
        }

        return response()->json(response_success('Berhasil mengubah order.', $orders), 200);
    }
}

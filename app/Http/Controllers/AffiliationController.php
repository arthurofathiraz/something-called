<?php

namespace App\Http\Controllers;

use App\Cart;
use App\Course;
use App\CourseAffiliation;
use App\Currency;
use App\InstructorSetting;
use App\Mail\SendOrderMail;
use App\Mail\WelcomeUser;
use App\Notifications\UserEnroll;
use App\Order;
use App\OrderAffiliation;
use App\PendingPayout;
use App\Setting;
use App\User;
use App\Wishlist;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Notification;
use Midtrans\Config as MidtransConfig;
use Midtrans\Snap;
use Session;
use Validator;

class AffiliationController extends Controller
{

    public function __construct()
    {
        /** Midtrans config **/
        // get data setting
        $setting = Setting::query()->first();

        /** Midtrans config **/
        if ($setting->enable_midtrans_production == 1) {
            // Set your Merchant Server Key
            MidtransConfig::$serverKey = env('MIDTRANS_SERVER_KEY_PRODUCTION');
            // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
            MidtransConfig::$isProduction = env('MIDTRANS_IS_PRODUCTION_PRODUCTION');
            // Set sanitization on (default)
            MidtransConfig::$isSanitized = env('MIDTRANS_IS_SANITIZED_PRODUCTION');
            // Set 3DS transaction for credit card to true
            MidtransConfig::$is3ds = env('MIDTRANS_IS_3DS_PRODUCTION');
        } else {
            // Set your Merchant Server Key
            MidtransConfig::$serverKey = env('MIDTRANS_SERVER_KEY');
            // Set to Development/Sandbox Environment (default). Set to true for Production Environment (accept real transaction).
            MidtransConfig::$isProduction = env('MIDTRANS_IS_PRODUCTION');
            // Set sanitization on (default)
            MidtransConfig::$isSanitized = env('MIDTRANS_IS_SANITIZED');
            // Set 3DS transaction for credit card to true
            MidtransConfig::$is3ds = env('MIDTRANS_IS_3DS');
        }
    }

    public function course_update(Request $request, $course_id = 0)
    {
        $request->validate([
            'commision_type' => 'required|in:fixed,percent',
            'amount' => 'required|numeric',
            'url' => 'required',
        ]);

        $input = $request->all();

        // input commision
        if (isset($input['commision_type']) && isset($input['amount']) && isset($input['url']) && $course_id != 0) {

            // set course id
            $input['course_id'] = $course_id;

            // input insert otherwise update
            $course_affiliation = CourseAffiliation::where('course_id', $course_id)->first();
            if ($course_affiliation) {
                $course_affiliation->update($input);
            } else {
                CourseAffiliation::create($input);
            }
        }

        Session::flash('success', trans('flash.UpdatedSuccessfully'));
        return redirect(url('course/create/' . $course_id));
    }

    public function redirect_product(Request $request, $user_id = 0, $course_id = 0, $slug = '')
    {
        $course = Course::where('id', $course_id)->first();
        if (!$course) {
            return redirect(url('/'))->with('delete', 'produk tidak di temukan');
        }

        // course affiliation
        $course_affiliation = CourseAffiliation::where('course_id', $course_id)->first();
        if (!$course_affiliation) {
            return redirect(url('/'))->with('delete', 'affiliation tidak di temukan');
        }

        if (empty($course_affiliation->url)) {
            return redirect(url('/'))->with('delete', 'landing page tidak di temukan');
        }

        $user = User::where('id', $user_id)->first();
        if (!$user) {
            return redirect(url('/'))->with('delete', 'user tidak di temukan');
        }

        if ($user->role != "affiliator") {
            return redirect(url('/'))->with('delete', 'user bukan affiliator');
        }

        // set cookie product
        // set cookie affiliator
        $request->session()->put('akl.aff.uid', Crypt::encrypt($user->id));
        $request->session()->put('akl.aff.ueml', Crypt::encrypt($user->email));
        $request->session()->put('akl.aff.cid', Crypt::encrypt($course->id));

        // redirect ke landing page
        return redirect($course_affiliation->url);
    }

    public function gotocheckout_product(Request $request, $user_id = 0, $course_id = 0, $slug = '')
    {
        $course = Course::where('id', $course_id)->first();
        if (!$course) {
            return redirect(url('/'))->with('delete', 'produk tidak di temukan');
        }

        // course affiliation
        $course_affiliation = CourseAffiliation::where('course_id', $course_id)->first();
        if (!$course_affiliation) {
            return redirect(url('/'))->with('delete', 'affiliation tidak di temukan');
        }

        if (empty($course_affiliation->url)) {
            return redirect(url('/'))->with('delete', 'landing page tidak di temukan');
        }

        $user = User::where('id', $user_id)->first();
        if (!$user) {
            return redirect(url('/'))->with('delete', 'user tidak di temukan');
        }

        if ($user->role != "affiliator") {
            return redirect(url('/'))->with('delete', 'user bukan affiliator');
        }

        // set cookie product
        // set cookie affiliator
        $request->session()->put('akl.aff.uid', Crypt::encrypt($user->id));
        $request->session()->put('akl.aff.ueml', Crypt::encrypt($user->email));
        $request->session()->put('akl.aff.cid', Crypt::encrypt($course->id));

        return redirect(url('product/' . $course_id . '/' . $slug . '/checkout'));
    }

    public function checkout_product(Request $request, $course_id = 0, $slug = '')
    {
        $course = Course::where('id', $course_id)->first();
        if (!$course) {
            return redirect(url('/'))->with('delete', 'produk tidak di temukan');
        }

        $social_proof = Order::query()->with('user', 'courses')->orderBy('created_at', 'desc')->limit(10)->get();

        foreach ($social_proof as $index => $sp) {
            $social_proof[$index]['user']['fname'] = masking($sp['user']['fname']);
            $social_proof[$index]['user']['lname'] = masking($sp['user']['lname']);
            $social_proof[$index]['pembelian'] = Carbon::parse($sp['created_at'])->diffForHumans();
        }

        return view('affiliation.checkout', compact('course', 'social_proof', 'course_id', 'slug'));
    }

    public function checkout_login(Request $request)
    {
        $rule = [
            'email' => 'required',
            'password' => 'required',
        ];

        // validate
        $validator = Validator::make($request->all(), $rule);
        if ($validator->fails()) {
            return redirect($request->url)->with('exception', implode("<br/>", $validator->messages()->all()));
        }

        $authUser = User::where('email', $request->email)->first();
        if (!$authUser) {
            return redirect($request->url)->with('exception', 'Email/passsword salah');
        }

        if (isset($authUser) && $authUser->status == 0) {
            return redirect($request->url)->with('exception', 'Blocked User');
        } else {

            $setting = Setting::first();

            if (isset($authUser)) {
                if ($setting->verify_enable == 0) {
                    Auth::login($authUser);
                    return redirect($request->url)->with('message', 'Berhasil login');
                } else {
                    if ($authUser->email_verified_at != NULL) {
                        Auth::login($authUser);
                        return redirect($request->url)->with('message', 'Berhasil login');
                    } else {
                        return redirect($request->url)->with('Verify your email');
                    }
                }
            } else {
                return redirect($request->url)->with('invalid User login');
            }
        }
    }

    public function checkout_logout(Request $request)
    {
        Auth::logout();
        return redirect($request->url)->with('message', 'Berhasil logout');
    }

    public function store_product(Request $request, $id_course = 0, $slug = '')
    {
        // needed data
        $setting = Setting::first();
        $currency = Currency::first();
        $courses_data = Course::where('id', $id_course)->first();

        // set keuntungan afiliasi dari cookie akl.aff
        // set transaction
        // set to midtrans url
        // untuk midtrans
        $midtrans_order_id = create_transaction_id();

        $list_order_item = [];
        $total_amount = Crypt::decrypt($request->total);

        // data
        $affiliation_user_id = (!empty($request->affiliation_user_id)) ? Crypt::decrypt($request->affiliation_user_id) : '';
        $affiliation_user_email = (!empty($request->affiliation_user_email)) ? Crypt::decrypt($request->affiliation_user_email) : '';

        // login
        if (!Auth::check()) {
            $rule = [
                'email' => 'required|email|unique:users,email',
                'name' => 'required',
                'mobile' => 'required|unique:users,mobile',
                'password' => 'required|min:8'
            ];

            // validate
            $validator = Validator::make($request->all(), $rule);
            if ($validator->fails()) {
                return redirect($request->url)->with('exception', implode("<br/>", $validator->messages()->all()));
            }

            $name = $request->name;
            $email = $request->email;
            $password = $request->password;
            $mobile = $request->mobile;

            // register user
            if ($setting->verify_enable == 0) {
                $verified = Carbon::now()->toDateTimeString();
            } else {
                $verified = NULL;
            }

            $user = User::create([
                'fname' => $name,
                'lname' => '',
                'email' => $email,
                'mobile' => $mobile,
                'email_verified_at' => $verified,
                'password' => Hash::make($password),
            ]);

            if ($setting->w_email_enable == 1) {
                try {
                    Mail::to($email)->send(new WelcomeUser($user));
                } catch (\Swift_TransportException $e) {

                }
            }
        } else {
            $user = User::where('id', Auth::user()->id)->first();
        }

        // set cart
        $cart = Cart::create([
            'user_id' => $user->id,
            'course_id' => $courses_data->id,
            'category_id' => $courses_data->category_id,
            'price' => $total_amount,
            'bundle_id' => 0,
            'cart_type' => 0,
//            'disamount' => $courses_data->price, // TODO discount
//            'distype' => $courses_data->price, // TODO discount
//            'coupon_id' => $courses_data->price, // TODO discount
        ]);


        // the rest of it
        $carts = Cart::where('user_id', $user->id)->get();

        foreach ($carts as $cart) {
            if ($cart->offer_price != 0) {
                $pay_amount = $cart->offer_price;
            } else {
                $pay_amount = $cart->price;
            }

            if ($cart->disamount != 0 || $cart->disamount != NULL) {
                $cpn_discount = $cart->disamount;
            } else {
                $cpn_discount = '';
            }

            $lastOrder = Order::orderBy('created_at', 'desc')->first();

            if (!$lastOrder) {
                // We get here if there is no order at all
                // If there is no number set it to 0, which will be 1 at the end.
                $number = 0;
            } else {
                $number = substr($lastOrder->order_id, 3);
            }

            if ($cart->type == 1) {
                $bundle_id = $cart->bundle_id;
                $bundle_course_id = $cart->bundle->course_id;
                $course_id = NULL;
                $duration = NULL;
                $instructor_payout = 0;
                $instructor_id = $cart->bundle->user_id;

                if ($cart->bundle->duration_type == "m") {

                    if ($cart->bundle->duration != NULL && $cart->bundle->duration != '') {
                        $days = $cart->bundle->duration * 30;
                        $todayDate = date('Y-m-d');
                        $expireDate = date("Y-m-d", strtotime("$todayDate +$days days"));
                    } else {
                        $todayDate = NULL;
                        $expireDate = NULL;
                    }
                } else {

                    if ($cart->bundle->duration != NULL && $cart->bundle->duration != '') {
                        $days = $cart->bundle->duration;
                        $todayDate = date('Y-m-d');
                        $expireDate = date("Y-m-d", strtotime("$todayDate +$days days"));
                    } else {
                        $todayDate = NULL;
                        $expireDate = NULL;
                    }

                }
            } else {

                if ($cart->courses->duration_type == "m") {

                    if ($cart->courses->duration != NULL && $cart->courses->duration != '') {
                        $days = $cart->courses->duration * 30;
                        $todayDate = date('Y-m-d');
                        $expireDate = date("Y-m-d", strtotime("$todayDate +$days days"));
                    } else {
                        $todayDate = NULL;
                        $expireDate = NULL;
                    }
                } else {

                    if ($cart->courses->duration != NULL && $cart->courses->duration != '') {
                        $days = $cart->courses->duration;
                        $todayDate = date('Y-m-d');
                        $expireDate = date("Y-m-d", strtotime("$todayDate +$days days"));
                    } else {
                        $todayDate = NULL;
                        $expireDate = NULL;
                    }

                }

                $setting = InstructorSetting::first();

                if ($cart->courses->instructor_revenue != NULL) {
                    $x_amount = $pay_amount * $cart->courses->instructor_revenue;
                    $instructor_payout = $x_amount / 100;
                } else {

                    if (isset($setting)) {
                        if ($cart->courses->user->role == "instructor") {
                            $x_amount = $pay_amount * $setting->instructor_revenue;
                            $instructor_payout = $x_amount / 100;
                        } else {
                            $instructor_payout = 0;
                        }

                    } else {
                        $instructor_payout = 0;
                    }
                }

                $bundle_id = NULL;
                $course_id = $cart->course_id;
                $bundle_course_id = NULL;
                $duration = $cart->courses->duration;
                $instructor_id = $cart->courses->user_id;
            }

            // add order id
            $order_id = '#' . sprintf("%08d", intval($number) + 1);

            // add order item
            $list_order_item[] = [
                'id' => $course_id,
                'price' => $pay_amount,
                'quantity' => 1,
                'name' => $cart->courses->title,
            ];

            $created_order = Order::create([
                    'course_id' => $course_id,
                    'user_id' => $user->id,
                    'instructor_id' => $instructor_id,
                    'order_id' => $order_id,
                    'transaction_id' => str_random(32),
                    'payment_method' => 'OnlinePayment',
                    'total_amount' => $pay_amount,
                    'coupon_discount' => $cpn_discount,
                    'currency' => $currency->currency,
                    'currency_icon' => $currency->icon,
                    'duration' => $duration,
                    'enroll_start' => $todayDate,
                    'enroll_expire' => $expireDate,
                    'status' => 0,
                    'bundle_id' => $bundle_id,
                    'bundle_course_id' => $bundle_course_id,
                    'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                    'midtrans_transaction_id' => $midtrans_order_id,
                ]
            );

            $created_order->save();

            // affiliation
            $course_affiliation = CourseAffiliation::where('course_id', $course_id)->first();
            if ($course_affiliation) {

                $affiliation_user = User::where('id', $affiliation_user_id)->first();
                if ($affiliation_user) {

                    // count affiliation amount
                    $affiliation_amount = 0;

                    if ($course_affiliation->commision_type == 'fixed') {
                        $affiliation_amount = $course_affiliation->amount;
                    } else if ($course_affiliation->commision_type == 'percent') {
                        $affiliation_percent_amount = $pay_amount * $course_affiliation->amount;
                        $affiliation_amount = $affiliation_percent_amount / 100;
                    }

                    OrderAffiliation::create([
                        'order_id' => $created_order->id,
                        'affiliation_user_id' => $affiliation_user->id,
                        'amount' => $affiliation_amount,
                        'status' => "PENDING",
                    ]);
                }
            }

            Wishlist::where('user_id', $user->id)->where('course_id', $cart->course_id)->delete();

            Cart::where('user_id', $user->id)->where('course_id', $cart->course_id)->delete();

            if ($instructor_payout != 0) {
                if ($created_order) {
                    if ($cart->type == 0) {
                        if ($cart->courses->user->role == "instructor") {
                            $created_payout = PendingPayout::create([
                                    'user_id' => $cart->courses->user_id,
                                    'course_id' => $cart->course_id,
                                    'order_id' => $created_order->id,
                                    'transaction_id' => $created_order->transaction_id,
                                    'total_amount' => $pay_amount,
                                    'currency' => $currency->currency,
                                    'currency_icon' => $currency->icon,
                                    'instructor_revenue' => $instructor_payout,
                                    'created_at' => \Carbon\Carbon::now()->toDateTimeString(),
                                    'updated_at' => \Carbon\Carbon::now()->toDateTimeString(),
                                ]
                            );
                        }
                    }
                }
            }

            if ($created_order) {
                if (env('MAIL_USERNAME') != null) {
                    try {

                        /*sending email*/
                        $x = 'You are successfully enrolled in a course';
                        $order = $created_order;
                        Mail::to($user->email)->send(new SendOrderMail($x, $order));

                    } catch (\Swift_TransportException $e) {

                    }
                }
            }

            if ($cart->type == 0) {

                if ($created_order) {
                    // Notification when user enroll
                    $cor = Course::where('id', $cart->course_id)->first();

                    $course = [
                        'title' => $cor->title,
                        'image' => $cor->preview_image,
                    ];

                    $enroll = Order::where('course_id', $cart->course_id)->get();

                    if (!$enroll->isEmpty()) {
                        foreach ($enroll as $enrol) {
                            $user_enroll = User::where('id', $enrol->user_id)->get();
                            Notification::send($user_enroll, new UserEnroll($course));
                        }
                    }
                }
            }

            // bikin snap redirect ke midtrans

            // transaction midtrans
            $transaction_details = array(
                'order_id' => $midtrans_order_id,
                'gross_amount' => $total_amount, // no decimal allowed for creditcard
            );

            // item midtrans
            $item_details = $list_order_item;

            // billing address midtrans
            $billing_address = array(
                'first_name' => $user->fname,
                'last_name' => $user->lname,
                'address' => $user->address,
                'phone' => $user->mobile,
                'email' => $user->email,
                'country_code' => 'IDN'
            );

            // customer details midtrans
            $customer_details = array(
                'first_name' => $user->fname,
                'last_name' => $user->lname,
                'address' => $user->address,
                'phone' => $user->mobile,
                'email' => $user->email,
                'country_code' => 'IDN'
            );

            // midtrans params
            $params = array(
                'transaction_details' => $transaction_details,
                'customer_details' => $customer_details,
                'item_details' => $item_details,
            );

            // Get Snap Payment Page URL
            $paymentUrl = Snap::createTransaction($params)->redirect_url;

            try {
                // Get Snap Payment Page URL
                $paymentUrl = Snap::createTransaction($params)->redirect_url;

                // Redirect to Snap Payment Page
                return redirect()->away($paymentUrl);
            } catch (Exception $e) {
                dd($e->getMessage());
            }

            return redirect($request->url)->with('exception', 'Error processing payment.');
        }
    }
}

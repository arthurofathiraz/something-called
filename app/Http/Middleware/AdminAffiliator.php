<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

class AdminAffiliator
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (Auth::User()->role == "admin" || Auth::User()->role == "affiliator") {
            return $next($request);
        } else {
            abort(404, 'Page Not Found.');
        }
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CourseAffiliation extends Model
{
    protected $table = 'course_affiliation';

    protected $fillable = ['course_id', 'commision_type', 'amount', 'url'];

    public function courses()
    {
        return $this->belongsTo('App\Course', 'course_id', 'id');
    }
}

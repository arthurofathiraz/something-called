<?php

use Carbon\Carbon;

function response_error($message = '')
{
    return [
        'status' => false,
        'message' => $message,
    ];
}

function response_success($message = '', $data = '')
{
    return [
        'status' => true,
        'message' => $message,
        'data' => $data
    ];
}

function create_transaction_id()
{
    // set date now
    $date = Carbon::now()->format('ymd');

    try {
        $string = random_bytes(32);
    } catch (TypeError $e) {
        // Well, it's an integer, so this IS unexpected.
        die("An unexpected error has occurred");
    } catch (Error $e) {
        // This is also unexpected because 32 is a reasonable integer.
        die("An unexpected error has occurred");
    } catch (Exception $e) {
        // If you get this message, the CSPRNG failed hard.
        die("Could not generate a random string. Is our OS secure?");
    }

    $token = substr(strtoupper(bin2hex($string)), 0, 10);

    return $date . $token;
}

function masking($str)
{
    $str_array = explode(" ", $str);

    foreach ($str_array as $index => $st) {
        if (!empty($st)) {
            $new_str = substr($st, 0, 1);
            $mask_str = '';
            for ($i = 0; $i < (strlen($st) - 1); $i++) {
                $mask_str .= '*';
            }

            $str_array[$index] = $new_str . $mask_str;
        }
    }

    return implode(" ", $str_array);
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderAffiliation extends Model
{
    protected $table = 'orders_affiliation';

    protected $fillable = ['order_id', 'affiliation_user_id', 'amount', 'status'];

    public function user()
    {
        return $this->belongsTo('App\User', 'affiliation_user_id', 'id');
    }

    public function orders()
    {
        return $this->belongsTo('App\Order', 'order_id', 'id');
    }
}
